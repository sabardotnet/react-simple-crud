import React, { useContext } from 'react';
import { useNavigate, Link } from "react-router-dom";
import { useState, useEffect } from 'react';
import axios from '../api/axios';
import Swal from "sweetalert2";
import styles from './Scissor.css';

const Scissor = () => {
    const navigate = useNavigate();

    const comSelection = ['rock', 'paper', 'scissor'];
    const [player, setPlayer] = useState({});
    const [winner, setWinner] = useState('VS');
    const [msg, setMsg] = useState('');
    const [score, setScore] = useState(0);
    const [comScore, setComScore] = useState(0);
    const [userPick, setUserPick] = useState('');
    const [comPick, setComPick] = useState('');
    const [roundFinish, setRoundFinish] = useState(false);
    const [roundPlay, setRoundPlay] = useState(0);
    const [gameFinish, setGameFinish] = useState(false);
  
    const updatePlayerScore = async (score) => {
      try {
        const updateScore = await axios.put(
          'http://localhost:3001/update-skor',
          {
            id: player.id,
            skor: score,
          },
          {
            headers: {
              Authorization: localStorage.getItem('accessToken'),
            },
          }
        );
        return updateScore;
      } catch (err) {
        console.log(err.message);
      }
    };
  
    useEffect(() => {
      let mounted = true;
      const whoAmI = async () => {
        try {
          const user = await axios('http://localhost:3001/whoami', {
            headers: {
              Authorization: localStorage.getItem('accessToken'),
            },
          });
          setPlayer(user.data);
        } catch (err) {
          if (err.response.status === 401) {
            navigate('/login');
          }
        }
      };
      if(mounted){
        whoAmI();
      }
      return () => mounted = false;
      console.log(player);
    }, []);
  
    useEffect(() => {
      determineWinner();
      if (roundFinish) {
        if (roundPlay === 3) {
          setGameFinish(true);
          setRoundFinish(false);
        }
        console.log({
          gameFinish,
          roundFinish,
          roundPlay,
          score,
          comScore,
          userPick,
          comPick,
          player,
        });
      }
    }, [roundFinish]);
  
    useEffect(() => {
      if (gameFinish) {
        const currentSkor = player.skor;
       
        if (score > comScore) {
          setWinner(` ${player.username} Win the Game!`);
          setPlayer({ ...player, skor: currentSkor + 1 });
          updatePlayerScore(currentSkor + 1);
          setMsg('Selamat skor anda '+(currentSkor))        
        } else if (comScore > score) {
          setWinner('COM Win the Game!');
          setMsg('')        
        } else {
          setWinner('its a draw!');
          setMsg('')        
        }

        Swal.fire({
            icon : 'info',
            text : winner+ ' Play again ? '+msg,
            showConfirmButton : true,
            confirmButtonText : 'Ya',
            showCancelButton : true,
            cancelButtonText : 'Tidak'
        }).then((hsl)=>{
            console.log(hsl, 'hasl')
            if(hsl.value == true){
                resetAll()
            }else{
                navigate('/')
            }
        }) 
        return
      }
    }, [gameFinish]);
  
    const hitApiForUpdateScore = () => {};
  
    const nextRound = () => {
      setWinner('VS');
      setUserPick('');
      setComPick('');
      setMsg('');
      setRoundFinish(false);
    };
  
    const getUserPick = (pick) => {
      console.log(pick, roundFinish, gameFinish, 'pick')
      if (!roundFinish && !gameFinish) {
      console.log(pick, 'pick2')

        setUserPick(pick);
        getComPick();
        if (roundPlay !== 3) {
          setRoundFinish(true);
        }
        setRoundPlay((state) => state + 1);
      }
    };
  
    const getComPick = () => {
      const randomNumber = Math.floor(Math.random() * 3);
      const randomPick = comSelection[randomNumber];
      setComPick(randomPick);
    };
  
    const determineWinner = () => {
      if (userPick === 'scissor') {
        if (comPick === 'scissor') {
          setWinner('Draw');
        } else if (comPick === 'rock') {
          setWinner('COM win');
          setComScore((state) => state + 1);
        } else if (comPick === 'paper') {
          setWinner(`${player.username} win`);
          setScore((state) => state + 1);
        }
      } else if (userPick === 'rock') {
        if (comPick === 'scissor') {
          setWinner(`${player.username} win`);
          setScore((state) => state + 1);
        } else if (comPick === 'rock') {
          setWinner('Draw');
        } else if (comPick === 'paper') {
          setWinner('COM win');
          setComScore((state) => state + 1);
        }
      } else if (userPick === 'paper') {
        if (comPick === 'scissor') {
          setWinner('COM win');
          setComScore((state) => state + 1);
        } else if (comPick === 'rock') {
          setWinner(`${player.username} win`);
          setScore((state) => state + 1);
        } else if (comPick === 'paper') {
          setWinner('Draw');
        }
      }
    };
  
    const resetAll = () => {
      setWinner('VS');
      setScore(0);
      setComScore(0);
      setUserPick('');
      setComPick('');
      setMsg('');
      setRoundFinish(false);
      setRoundPlay(0);
      setGameFinish(false);
    };
  
    return (
        <>
        <section id="sec_scissor" className="skills">
            <div className="section-title">
            <h2>ROCK PAPER SCISSORS</h2>
            <p>Silahkan mainkan</p>
            </div>           
            <div className="container" data-aos="fade-up">
                <div className='row'>
                    <div className='col col-xs-3 my-col my-col'>
                        <h3>{player.username}</h3>
                    </div>
                    <div className='col col-xs-3 my-col my-col'>
                    </div>
                    <div className='col col-xs-3 my-col  my-col'>
                        <h3>COM</h3>
                    </div>                                        
                </div>                
                <div className='row'>
                    <div className={`col col-xs-3 my-col my-col ${ userPick === 'paper' ? 'chosen' : null}`} onClick={() => getUserPick('paper')}>
                        <img  src={"assets/img/kertas.png"} alt="" />
                    </div>
                    <div className='col col-xs-3 my-col my-col'>
                    </div>
                    <div className={`col col-xs-3 my-col my-col ${ comPick === 'paper' ? 'chosen' : null}`}>
                        <img  src={"assets/img/kertas.png"} alt="" />
                    </div>                                        
                </div>
                <div className='row'>
                    <div className={`col col-xs-3 my-col my-col ${ userPick === 'scissor' ? 'chosen' : null}`} onClick={() => getUserPick('scissor')}>
                        <img  src={"assets/img/gunting.png"} alt="" />
                    </div>
                    <div className='col col-xs-3 my-col result'>
                        <h1>{winner}</h1>
                    </div>
                    <div className={`col col-xs-3 my-col my-col ${ comPick === 'scissor' ? 'chosen' : null}`}>
                        <img  src={"assets/img/gunting.png"} alt="" />
                    </div>                                        
                </div>
                <div className='row'>
                    <div className={`col col-xs-3 my-col my-col ${ userPick === 'rock' ? 'chosen' : null}`} onClick={() => getUserPick('rock')}>
                        <img  src={"assets/img/batu.png"} alt="" />
                    </div>
                    <div className='col col-xs-3 my-col'>
                        {roundFinish && <img className={styles.refresh} src="assets/img/refresh.png" alt="" onClick={nextRound} disabled={gameFinish} />}
                        {/* <img  src={"assets/img/refresh.png"} alt="" /> */}
                    </div>
                    <div className={`col col-xs-3 my-col my-col ${ comPick === 'rock' ? 'chosen' : null}`}>
                        <img  src={"assets/img/batu.png"} alt="" />
                    </div>                                        
                </div>                                
            </div>
        </section>
        </>
    );
};

export default Scissor;