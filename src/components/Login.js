import React, { useContext } from 'react';
import { useNavigate } from "react-router-dom";
import { useState } from 'react';
import axios from '../api/axios';
import Swal from "sweetalert2";

const Login = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate = useNavigate();
  
    const loginHandler = async (e) => {
        e.preventDefault();
        try {
            

      const login = await axios.post('http://localhost:3001/login', {
        email,
        password,
      });
  
      localStorage.setItem('accessToken', login.data.token);
      console.log(localStorage);
  
      if (!login.data.token) {
        navigate('/login');
        Swal.fire({  
            title: 'Error',  
            type: 'error',  
            text: "User not found",  
          });
        return;
      }
      navigate('/');
    } catch (error) {
        Swal.fire({  
            title: 'Error',  
            type: 'error',  
            text: "Something error try again",  
          });  
    }
    };

  return (
    <section id="contact" className="contact">
          <div className="section-title">
            <h2>Login</h2>
          </div>
      <div className="container" data-aos="fade-up">
        <div className='row justify-content-center'>
        <div className="col-lg-6 col-md-6">
            <form className="php-email-form"  style={{  margin : '0 auto' }}>
                <div className="row">
                    <div className="form-group col-md-12">
                    <label htmlFor="name">Email</label>
                    <input type="email" name="email" autoComplete="off" className="form-control" id="email" required placeholder="ex:John@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="name">Password</label>
                    <input type="password" autoComplete="off" className="form-control" name="password" id="password" required placeholder="ex:1234" value={password} onChange={(e) => setPassword(e.target.value)}/>
                </div>
                <div className="text-center">
                    <button type="submit" className='ok-btn' onClick={loginHandler}>Login</button>
                </div>
                <div className="text-center" style={{ marginTop : '10px' }}>
                  <p>Belum punya akun ? </p>
                  <a href="/register" className="buy-btn">Register</a>
                </div> 
                <div className='back_to_home'>
                    <a href="/"><i className="ri-arrow-left-circle-fill my-icon"></i></a>
                </div>               
                </form>
            </div>
        </div>
    </div>
    </section>

  );
};

export default Login;