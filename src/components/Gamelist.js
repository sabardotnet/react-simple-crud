import React from 'react';
import { useNavigate, Link } from 'react-router-dom';
// import axios from '../api/axios';
// import { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

const gamelist = () => {
  return (
    <>
      <section id="game-list" className="skills">
        <div className="section-title">
          <h2>Game List </h2>
          <p>Recomended Game For You</p>
        </div>
        <div className="row text-center justify-conten-center p-3">
          <div className="col col-12 col-sm-12 col-md-3 col-lg-3">
            <Card>
              <Card.Img variant="top" src="assets/img/gamecard/suwit-game.png" />
              <Card.Body>
                <Card.Title>Jankenpon</Card.Title>
                <Card.Text>Game Suwit yang di mainkan oleh 2 tangan orang pemain</Card.Text>
                <Link to='/scissor'>
                  <Button variant="warning">Play game</Button>
                </Link>
              </Card.Body>
            </Card>
          </div>
          <div className="col col-12 col-sm-12 col-md-3 col-lg-3">
            <Card>
              <Card.Img variant="top" src="assets/img/gamecard/loading.png" />
              <Card.Body>
                <Card.Title>NEW GAME</Card.Title>
                <Card.Text>Ini adalah game baru</Card.Text>
                <Button variant="warning" disabled>Play game</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col col-12 col-sm-12 col-md-3 col-lg-3">
            <Card>
              <Card.Img variant="top" src="assets/img/gamecard/coming-soon.jpg" />
              <Card.Body>
                <Card.Title>COMING SOON</Card.Title>
                <Card.Text>Add Game</Card.Text>
                <Button variant="warning" disabled>play game</Button>
              </Card.Body>
            </Card>
          </div>
          <div className="col col-12 col-sm-12 col-md-3 col-lg-3">
            <Card>
              <Card.Img variant="top" src="assets/img/gamecard/coming-soon.jpg" />
              <Card.Body>
                <Card.Title>COMING SOON</Card.Title>
                <Card.Text>Add Game</Card.Text>
                <Button variant="warning" disabled>play game</Button>
              </Card.Body>
            </Card>
          </div>
        </div>
      </section>
    </>
  );
};
export default gamelist;
