import React, { useContext } from 'react';
import PlayerForm from './PlayerForm';
import { useParams } from 'react-router-dom';
import PlayersContext from '../context/PlayersContext';
import { useNavigate } from "react-router-dom";

const EditPlayer = ( history ) => {
  const navigate = useNavigate();
  const { players, setPlayers } = useContext(PlayersContext);
  const { id } = useParams();
  const playerToEdit = players.find((player) => player.id === id);

  const handleOnSubmit = (player) => {
    const filteredPlayers = players.filter((player) => player.id !== id);
    setPlayers([player, ...filteredPlayers]);
    navigate('/');
  };

  return (
    <div>
      <PlayerForm player={playerToEdit} handleOnSubmit={handleOnSubmit} />
    </div>
  );
};

export default EditPlayer;
