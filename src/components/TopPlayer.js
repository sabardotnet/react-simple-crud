import React from 'react';
import { useNavigate } from 'react-router-dom';
import axios from '../api/axios';
import { useState, useEffect } from 'react'

const TopPlayer =  ()=>{
  const [topPlayer, setTopPlayer] = useState([]);
  useEffect( () => {
    let mounted = true;
    async function fetchData() {
      const response = await axios.get('http://localhost:3001/topplayer').then((data)=>{
        console.log(data, 'data')
        setTopPlayer(data.data);
      }).catch((err)=>{
      
      })
    }
    if(mounted){
      fetchData();
    }
    return () => mounted = false;
  }, []);

    return (
        <>
      <section id="leader-board" className="skills">
      <div className="section-title">
          <h2>Top Player</h2>
          <p>Berikut adalah 10 top player</p>
        </div>
      <div className="container" data-aos="fade-up">

        <div className="row">
          <div className="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="assets/img/top_player.png" className="img-fluid" alt="" />
          </div>
          <div className="col-lg-6" data-aos="fade-left" data-aos-delay="100">
              {
                topPlayer.length > 0 ? 
                topPlayer.map((val, index) => {
                  return (
                  <div key={index}>
                  <div className='my-box' >
                    <div className='row'>
                      <div className='col-md-6 text-left'>
                        <p>{val.username}</p>
                      </div>
                      <div className='col-md-6 text-right'>
                        <h4>{val.skor}</h4>
                      </div>
                    </div>
                  </div>
                  </div>
                  );
                  
                }) : 
                <>
                <div className='' style={{ display:'flex', alignItems : 'center', height:'100%', verticalAlign:'middle' }}>
                      <h4>Yah...! datanya masih kosong</h4>
                </div>
                </>
              }
              

                                              
          </div>
        </div>

      </div>
    </section>
        </>
    );
}
export default TopPlayer;
