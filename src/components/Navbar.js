import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { useNavigate } from 'react-router-dom';
import style from './Navbar.css';

const HeaderMenu = () => {
  const token = localStorage.getItem('accessToken');
  const navigate = useNavigate();

  function logout() {
    localStorage.clear();
    navigate('/');
  }
  return (
    <>
      <header id="header">
        <Container>
          <Navbar bg="warning" className="fixed-top " expand="lg">
            <Navbar.Brand href="/" className="name-grup">
              Game-4Grup<i className="ri-game-fill"></i>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="navbar-1" className="my-navbar">
              <Nav className="ms-auto text-center listMenu ">
                <Nav.Link href="#our-team" className='scrollto'>
                  <i className="ri-team-line"></i> Our-Team
                </Nav.Link>
                <Nav.Link href="#leader-board" className='scrollto'>
                 Leader Board<i className="ri-dashboard-3-line"></i>
                </Nav.Link>
                <Nav.Link href="/playerlist" className='scrollto'>
                 Player<i className="ri-dashboard-3-line"></i>
                </Nav.Link>                
                <Nav.Link href="#game-list" className='scrollto'>
                  Games<i className="ri-gamepad-line"></i>
                </Nav.Link>
                {token != null && token != '' && token !== undefined ? (
                  <>
                    <Nav.Link href="/profile">Profile</Nav.Link>
                    <Nav.Link href="#" onClick={logout}>
                      Logout
                    </Nav.Link>
                  </>
                ) : (
                  <>
                    <Nav.Link href="/login">
                      <i className="ri-login-box-line"></i>Login
                    </Nav.Link>
                    <Nav.Link href="/register">
                      <i className="ri-registered-line"></i>Register
                    </Nav.Link>
                  </>
                )}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
        </Container>
      </header>
    </>
  );
};

export default HeaderMenu;
