import React from 'react';
import { useNavigate } from 'react-router-dom';
import axios from '../api/axios';
import { useState, useEffect } from 'react'

const PlayerList =  ()=>{
  const [playerList, setPlayerList] = useState([]);
  useEffect( () => {
    let mounted = true;
    async function fetchData() {
      const response = await axios.get('http://localhost:3001/datauser').then((data)=>{
        setPlayerList(data.data);
      }).catch((err)=>{
      })
    }
    if(mounted){
      fetchData();
    }
    return () => mounted = false;
  }, []);

    return (
        <>
      <section id="player-list" className="skills">
      <div className="section-title">
          <h2>Player</h2>
        </div>
      <div className="container" data-aos="fade-up">
        <div className="row">
              {
                playerList.length > 0 ? 
                playerList.map((val, index) => {
                  return (
                  <div className="col-lg-4 col-md-3 col-sm-12" data-aos="fade-left" data-aos-delay="100">
                    <div key={index}>
                      <div className='my-box' >
                        <div className='row'>
                          <div className='col-md-4 text-left'>
                            Username
                          </div>  
                          <div className='col-md-2 text-left'>
                            :
                          </div>                                                   
                          <div className='col-md-6 text-left'>
                            <p>{val.username}</p>
                          </div>
                        </div>
                        <div className='row'>
                          <div className='col-md-4 text-left'>
                            Email
                          </div>  
                          <div className='col-md-2 text-left'>
                            :
                          </div>                                                   
                          <div className='col-md-6 text-left'>
                            <p>{val.email}</p>
                          </div>
                        </div>
                        <div className='row'>
                          <div className='col-md-4 text-left'>
                            Skor
                          </div>  
                          <div className='col-md-2 text-left'>
                            :
                          </div>                                                   
                          <div className='col-md-6 text-left'>
                            <p>{val.skor}</p>
                          </div>
                        </div>                        
                      </div>
                    </div>
                  </div>
                  );
                  
                }) : 
                <>
                <div className='col-md-12 col-lg-12' style={{ display:'flex', alignItems : 'center', height:'100%', verticalAlign:'middle' }}>
                      <h4>Yah...! datanya masih kosong</h4>
                </div>
                </>
              }                       
        </div>
      </div>
    </section>
        </>
    );
}
export default PlayerList;
