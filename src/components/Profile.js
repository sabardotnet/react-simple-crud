import React, { useContext } from 'react';
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from 'react';
import axios from '../api/axios';
import Swal from "sweetalert2";

const Profile = () => {
    const [dataUser, setdataUser] = useState({});
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [skor, setSkor] = useState('');
    const [id, setId] = useState('');


    useEffect( () => {
      let mounted = true;
      async function fetchData() {
        const response = await axios.get('http://localhost:3001/whoami', {
          headers : {
            Authorization: localStorage.getItem('accessToken'),
          }
        }).then((data)=>{
          setEmail(data.data.email);
          setUsername(data.data.username);
          setSkor(data.data.skor);
          setId(data.data.id);
        }).catch((err)=>{
        
        })
      }
      if(mounted){
        fetchData();
      }
      return () => mounted = false;
    }, []);

    const navigate = useNavigate();

    const registerHandler = async (e) => {
  e.preventDefault();
      var param = {
        username : username, 
        email : email,
        id : id
      }
      if(password!=''){
        param['password'] = password;
      }
      const register = await axios.put('http://localhost:3001/edit', 
      param,
      {
        headers: {
          Authorization: localStorage.getItem('accessToken'),
        },
      } ).then((success)=>{
        Swal.fire({  
          title: 'Success',  
          type: 'success',  
          text: success.data.message,  
        }).then((val)=>{
          navigate('/');
        })
      }).catch((err)=>{
        console.log(err, 'error')
        if(err && err.response.status == 422){
          Swal.fire({  
            title: 'Error',  
            type: 'error',  
            text: err.response.data.message,  
          });
        }
      })
  
    };
  return (
    <main id="main">
    <section id="contact" className="contact">
      <div className="container" data-aos="fade-up">
        <div className="section-title">
          <h2>Profile Anda</h2>
        </div>
        <div className='row justify-content-center'>

            <div className="col-lg-6 col-md-6 d-flex align-items-center">
              <form className="php-email-form" style={{  }} autoComplete="off">
                  <div className="row">
                      <div className="form-group col-md-12">
                      <label htmlFor="name">Username</label>
                      <input type="text" name="username" autoComplete="off" className="form-control" id="username" required placeholder="ex:User" value={username} onChange={(e) => setUsername(e.target.value)}/>
                      </div>
                  </div>              
                  <div className="row">
                      <div className="form-group col-md-12">
                      <label htmlFor="name">Email</label>
                      <input type="email" name="email" autoComplete="off" className="form-control" id="email" required placeholder="ex:John@gmail.com" value={email} onChange={(e) => setEmail(e.target.value)}/>
                      </div>
                  </div>
                  <div className="row">
                      <div className="form-group col-md-12">
                      <label htmlFor="name">Skor</label>
                      <input type="skor" readOnly className="form-control" id="skor" required placeholder="" value={skor} />
                      </div>
                  </div>                  
                  <div className="form-group">
                      <label htmlFor="name">Password</label>
                      <input type="password" autoComplete="off" className="form-control" name="password" id="password" required placeholder="Rubah password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                  </div>
                  <div className="text-center">
                      <button type="submit" onClick={registerHandler}>Update Profile</button>
                  </div>
                  <div className='back_to_home'>
                      <a href="/"><i className="ri-arrow-left-circle-fill my-icon"></i></a>
                  </div>                  
                  </form>
              </div>
        </div>
    </div>
    </section>
    </main>

  );
};

export default Profile;