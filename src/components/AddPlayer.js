import React, { useContext } from 'react';
import BookForm from './PlayerForm';
import PlayersContext from '../context/PlayersContext';
import { useNavigate } from "react-router-dom";

const AddPlayer = ({history} ) => {
  const { players, setPlayers } = useContext(PlayersContext);
  const navigate = useNavigate();
  const handleOnSubmit = (player) => {
    setPlayers([player, ...players]);
    navigate('/');
  };

  return (
    <React.Fragment>
      <BookForm handleOnSubmit={handleOnSubmit} />
    </React.Fragment>
  );
};

export default AddPlayer;
