import React from 'react';
import { useNavigate } from 'react-router-dom';
import Navbar from './Navbar';
import Container from 'react-bootstrap/Container';
import TopPlayer from './TopPlayer';
import Gamelist from './Gamelist';

const Home = () => {
  return (
    <>
      <Navbar />
      <section id="hero" className="d-flex align-items-center">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
              <h1>Hi, Welcome To Binar Challenge 9 </h1>
              <h2>Mau maen game seru ? klik play game</h2>
              <div className="d-flex justify-content-center justify-content-lg-start">
                <a href="#game-list" className="btn-get-started scrollto">
                  Play Game
                </a>
              </div>
            </div>
            <div className="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
              <img src="assets/img/hero-img.png" className="img-fluid animated" alt="" />
            </div>
          </div>
        </div>
      </section>
      <main id="main">
        {/* <section id="clients" className="clients section-bg">
          <div className="container">
            <div className="row" data-aos="zoom-in">
              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-1.png" className="img-fluid" alt="" />
              </div>

              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-2.png" className="img-fluid" alt="" />
              </div>

              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-3.png" className="img-fluid" alt="" />
              </div>

              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-4.png" className="img-fluid" alt="" />
              </div>

              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-5.png" className="img-fluid" alt="" />
              </div>

              <div className="col-lg-2 col-md-4 col-6 d-flex align-items-center justify-content-center">
                <img src="assets/img/clients/client-6.png" className="img-fluid" alt="" />
              </div>
            </div>
          </div>
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
            <path
              fill="#ebaf42"
              fillOpacity="1"
              d="M0,256L30,218.7C60,181,120,107,180,117.3C240,128,300,224,360,245.3C420,267,480,213,540,197.3C600,181,660,203,720,213.3C780,224,840,224,900,208C960,192,1020,160,1080,138.7C1140,117,1200,107,1260,138.7C1320,171,1380,245,1410,282.7L1440,320L1440,0L1410,0C1380,0,1320,0,1260,0C1200,0,1140,0,1080,0C1020,0,960,0,900,0C840,0,780,0,720,0C660,0,600,0,540,0C480,0,420,0,360,0C300,0,240,0,180,0C120,0,60,0,30,0L0,0Z"
            ></path>
          </svg>
        </section> */}
        <Gamelist />
        <TopPlayer />
        <section id="our-team" className="team section-bg">
          <div className="container" data-aos="fade-up">
            <div className="section-title">
              <h2>Our Team</h2>
              <p>kegiatan terdiri dari 4 kelompok, Tujuan Game ini adalah membuat challenge game di capter-9. Game ini bisa dilakukan oleh 2 tangan orang pemain, permainan ini disebut Jankenpon/Suwit Jepang.</p>
            </div>
            <div className="row">
              <div className="col-lg-6">
                <div className="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                  <div className="pic">
                    <img src="assets/img/team/foto4.jpg" className="img-fluid" alt="" />
                  </div>
                  <div className="member-info">
                    <h4>Adli Hardiyanto</h4>
                    <span>Developer</span>
                    <p>bekerja bagian Back-End </p>
                    <div className="social">
                      <a href="">
                        <i className="ri-twitter-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-facebook-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-instagram-fill"></i>
                      </a>
                      <a href="">
                        {' '}
                        <i className="ri-linkedin-box-fill"></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 mt-4 mt-lg-0">
                <div className="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="200">
                  <div className="pic">
                    <img src="assets/img/team/foto2.jpg" className="img-fluid" alt="" />
                  </div>
                  <div className="member-info">
                    <h4>Suhairoh</h4>
                    <span>Developer</span>
                    <p>Bekerja Bagian Front-End </p>
                    <div className="social">
                      <a href="">
                        <i className="ri-twitter-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-facebook-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-instagram-fill"></i>
                      </a>
                      <a href="">
                        {' '}
                        <i className="ri-linkedin-box-fill"></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 mt-4">
                <div className="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="300">
                  <div className="pic">
                    <img src="assets/img/team/foto4.jpg" className="img-fluid" alt="" />
                  </div>
                  <div className="member-info">
                    <h4>Ahmad Muzaki Syafii</h4>
                    <span>Developer </span>
                    <p>mengerjakan bagian Back-End</p>
                    <div className="social">
                      <a href="">
                        <i className="ri-twitter-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-facebook-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-instagram-fill"></i>
                      </a>
                      <a href="">
                        {' '}
                        <i className="ri-linkedin-box-fill"></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-6 mt-4">
                <div className="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="400">
                  <div className="pic">
                    <img src={'assets/img/team/foto4.jpg'} className="img-fluid" alt="" />
                  </div>
                  <div className="member-info">
                    <h4>Akmal Galuh</h4>
                    <span>Developer</span>
                    <p>Bekerja Bagian Front-End </p>
                    <div className="social">
                      <a href="">
                        <i className="ri-twitter-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-facebook-fill"></i>
                      </a>
                      <a href="">
                        <i className="ri-instagram-fill"></i>
                      </a>
                      <a href="">
                        {' '}
                        <i className="ri-linkedin-box-fill"></i>{' '}
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320">
          <path
            fill="#ebaf42"
            fillOpacity="1"
            d="M0,160L60,186.7C120,213,240,267,360,261.3C480,256,600,192,720,186.7C840,181,960,235,1080,245.3C1200,256,1320,224,1380,208L1440,192L1440,320L1380,320C1320,320,1200,320,1080,320C960,320,840,320,720,320C600,320,480,320,360,320C240,320,120,320,60,320L0,320Z"
          ></path>
        </svg>
      </main>
      <footer id="footer">
        <div className="container footer-bottom clearfix">
          <div className="copyright">
            &copy; Copyright{' '}
            <strong>
              <span>2022</span>
            </strong>
            . Game 4 Grup
          </div>
          <div className="credits">
            Create By <i className="ri-heart-3-fill" style={{ color: 'red' }}></i>
            <a href="https://www.instagram.com/zuhairah_handmade/" style={{ color: 'black' }}>
              Zuhairah
            </a>
          </div>
        </div>
      </footer>
    </>
  );
};
export default Home;
