import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import Header from '../components/Header';
import AddPlayer from '../components/AddPlayer';
import PlayersList from '../components/PlayersList';
import Home from '../components/Home';
import Login from '../components/Login';
import useLocalStorage from '../hooks/useLocalStorage';
import EditPlayer from '../components/EditPlayer';
import BooksContext from '../context/PlayersContext';
import { useState, useEffect } from 'react';
import AOS from 'aos';
import Register from '../components/Register';
import Profile from '../components/Profile';
import Scissor from '../components/Scissor';
import PlayerList from '../components/Playerlist';

const AppRouter = () => {
  const [players, setPlayers] = useLocalStorage('players', []);
  useEffect(() => {

    
    
      /**
       * Easy selector helper function
       */
      const select = (el, all = false) => {
        el = el.trim()
        if (all) {
          return [...document.querySelectorAll(el)]
        } else {
          return document.querySelector(el)
        }
      }
    
      /**
       * Easy event listener function
       */
      const on = (type, el, listener, all = false) => {
        let selectEl = select(el, all)
        if (selectEl) {
          if (all) {
            selectEl.forEach(e => e.addEventListener(type, listener))
          } else {
            selectEl.addEventListener(type, listener)
          }
        }
      }
    
      /**
       * Easy on scroll event listener 
       */
      const onscroll = (el, listener) => {
        el.addEventListener('scroll', listener)
      }
    
      /**
       * Navbar links active state on scroll
       */
      let navbarlinks = select('#navbar .scrollto', true)
      const navbarlinksActive = () => {
        let position = window.scrollY + 200
        navbarlinks.forEach(navbarlink => {
          if (!navbarlink.hash) return
          let section = select(navbarlink.hash)
          if (!section) return
          if (position >= section.offsetTop && position <= (section.offsetTop + section.offsetHeight)) {
            navbarlink.classList.add('active')
          } else {
            navbarlink.classList.remove('active')
          }
        })
      }
      window.addEventListener('load', navbarlinksActive)
      onscroll(document, navbarlinksActive)
    
      /**
       * Scrolls to an element with header offset
       */
      const scrollto = (el) => {
        let header = select('#header')
        let offset = header.offsetHeight
    
        let elementPos = select(el).offsetTop
        window.scrollTo({
          top: elementPos - offset,
          behavior: 'smooth'
        })
      }
    
      /**
       * Toggle .header-scrolled class to #header when page is scrolled
       */
      let selectHeader = select('#header')
      if (selectHeader) {
        const headerScrolled = () => {
          if (window.scrollY > 100) {
            selectHeader.classList.add('header-scrolled')
          } else {
            selectHeader.classList.remove('header-scrolled')
          }
        }
        window.addEventListener('load', headerScrolled)
        onscroll(document, headerScrolled)
      }
    
      /**
       * Back to top button
       */
      let backtotop = select('.back-to-top')
      if (backtotop) {
        const toggleBacktotop = () => {
          if (window.scrollY > 100) {
            backtotop.classList.add('active')
          } else {
            backtotop.classList.remove('active')
          }
        }
        window.addEventListener('load', toggleBacktotop)
        onscroll(document, toggleBacktotop)
      }
    
      /**
       * Mobile nav toggle
       */
      on('click', '.mobile-nav-toggle', function(e) {
        select('#navbar').classList.toggle('navbar-mobile')
        this.classList.toggle('bi-list')
        this.classList.toggle('bi-x')
      })
    
      /**
       * Mobile nav dropdowns activate
       */
      on('click', '.navbar .dropdown > a', function(e) {
        if (select('#navbar').classList.contains('navbar-mobile')) {
          e.preventDefault()
          this.nextElementSibling.classList.toggle('dropdown-active')
        }
      }, true)
    
      /**
       * Scrool with ofset on links with a class name .scrollto
       */
      on('click', '.scrollto', function(e) {
        if (select(this.hash)) {
          e.preventDefault()
    
          let navbar = select('#navbar')
          // if (navbar.classList.contains('navbar-mobile')) {
          //   navbar.classList.remove('navbar-mobile')
          //   let navbarToggle = select('.mobile-nav-toggle')
          //   navbarToggle.classList.toggle('bi-list')
          //   navbarToggle.classList.toggle('bi-x')
          // }
          scrollto(this.hash)
        }
      }, true)


    AOS.init({
      duration: 1000,
      easing: 'ease-in-out',
      once: true,
      mirror: false,
    });
    let preloader = select('#preloader');
    if (preloader) {
      window.addEventListener('load', () => {
        preloader.remove()
      });
    }

  });
  return (
    <BrowserRouter>
      <div>
        <div>
          <BooksContext.Provider value={{ players, setPlayers }}>
            <Routes>
              <Route element={<Home/>} path="/" />
              <Route element={<AddPlayer/>} path="/add" />
              <Route element={<Login/>} path="/login" />
              <Route element={<Register/>} path="/register" />
              <Route element={<Profile/>} path="/profile" />
              <Route element={<PlayerList/>} path="/playerlist" />
              <Route element={<Scissor/>} path="/scissor" />
              <Route element={<EditPlayer/>} path="/edit/:id" />
              <Route element={() => <Navigate to="/" />} />
            </Routes>
          </BooksContext.Provider>
        </div>
      </div>
    </BrowserRouter>
  );
};

export default AppRouter;
